app.config(function($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise("/client");

  $stateProvider
    .state('contacts', {
      url: '/contacts',
      templateUrl: 'templates/contacts/contactsTable.html',
      controller: 'ContactsController'
    })
    .state('contacts.view', {
      url: '/view/:family_id',
      templateUrl: 'templates/contacts/viewContacts.html',
      controller: 'ContactViewController'
    })
})