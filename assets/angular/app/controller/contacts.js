app.controller('ContactsController', function($scope, $rootScope, $filter, Auth, Contacts){

  $scope.key = null

  $scope.contactFilter = {startWith: '*', page: 1, count: 15, sortColumn: null, sortMode: null, searchCriteria: null, token: null}
  $scope.contacts = []
  $scope.getContacts = function(){
    $scope.contactFilter.token = $scope.key
    Contacts.getContacts($scope.contactFilter, function(res){
      $scope.contacts = res
    })
  }

  $scope.currentAlpha = null
  $scope.filterContact = function(startWith, page, count, sortColumn, sortMode){
    $scope.currentAlpha = startWith
    $scope.contacts = []
    $scope.contactFilter.startWith = startWith
    $scope.contactFilter.page = page
    $scope.contactFilter.count = count
    $scope.contactFilter.sortColumn = sortColumn
    $scope.contactFilter.sortMode = sortMode
    $scope.getContacts()
  }

  Auth.login({username: 'testuser6', password: 'Nzfsg996'}, function(res){
    $scope.key = res.list
    $scope.getContacts()
  })

  $scope.alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('')

})

app.controller('ContactViewController', function($scope, $filter, $rootScope, $stateParams, Auth, Contacts){

  $scope.key = null

  $scope.clients = []
  $scope.client = {}
  $scope.children = []
  $scope.getClientInfo = function(clientId){
    Contacts.getClientInfo({token: $scope.key, familyID: $stateParams.family_id, clientId: clientId}, function(res){
      $scope.clients = res
      $scope.client = res.slice(-1)[0]
      $scope.children = $filter('orderBy')(res, 'Role')
    })
  }

  $scope.familyInfo = []
  $scope.getFamilyInfo = function(){
    Contacts.getContactFamilyInfo({token: $scope.key, familyID: $stateParams.family_id}, function(res){
      $scope.familyInfo = res
    })
  }

  $scope.loans = []
  $scope.getLoanInfo = function(){
    Contacts.getContactsLoanList({token: $scope.key, familyID: $stateParams.family_id}, function(res){
      $scope.loans = res
    })
  }

  $scope.familyAddress = []
  $scope.getFamilyAddressInfo = function(){
    Contacts.getFamilyAddressInfo({token: $scope.key, familyID: $stateParams.family_id}, function(res){
      $scope.familyAddress = res
    })
  }

  $scope.notes = []
  $scope.getNotes = function(){
    Contacts.getNotes({token: $scope.key, familyID: $stateParams.family_id}, function(res){
      $scope.notes = res
    })
  }

  $scope.newNote = {}
  $scope.setNote = function(){
    $scope.newNote.familyID = $stateParams.family_id
    Contacts.setNotes({token: $scope.key}, $scope.newNote, function(){
      $scope.getNotes()
      $scope.newNote = {}
      $('.modal').modal('hide')
    })
  }

  $scope.relationships = []
  $scope.getRelations = function(){
    Contacts.getRelations({token: $scope.key, familyID: $stateParams.family_id}, function(res){
      $scope.relationships = res
    })
  }

  $scope.entityType = [
    'Trust',
    'LTC (Look Through Company)',
    'Company',
    'Partnership',
    'Referrer',
    'Trustee',
    'Accountant',
    'Guarantor',
    'Solicitor',
    'Real Estate Agent',
    'Financial Advisor',
    'Insurance Broker',
    'Other'
  ]
  $scope.newRelationship = {OrganisationType: [], Email: [], Phone: []}
  $scope.setRelationship = function(){
    $scope.newRelationship.familyID = $stateParams.family_id
    Contacts.setRelations({token: $scope.key}, $scope.newRelationship, function(){
      $scope.getRelations()
      $scope.newRelationship = {}
      $('.modal').modal('hide')
    })
  }

  $scope.initSearch = function (elem) {
    var address = new google.maps.places.Autocomplete(document.getElementById(elem), {
      language: 'en-GB',
      types: ['geocode']
    });
    address.addListener('place_changed', function () {
      var place = address.getPlace();
      if (elem == 'searchAddr'){
        $scope.newRelationship.Address = []
        // var addr = {
        //   formatted_address: place.formatted_address,
        //   latitude: place.geometry.location.lat(),
        //   longitude: place.geometry.location.lng()
        // }
        // angular.forEach(place.address_components, function(value, key){
        //   addr[value.types[0]] = value.long_name
        // })
        // $scope.newRelationship.Address[0] = addr
      }
      $scope.$digest()
    })
  }

  $scope.newClient = {}
  $scope.newClient = function(){
    Contacts.setClientInfo($scope.newClient, function(res){
      $scope.getClientInfo()
    })
  }

  $scope.tags = []
  $scope.getTag = function(){
    Contacts.getTag({token: $scope.key, familyID: $stateParams.family_id}, function(res){
      $scope.tags = res
    })
  }

  $scope.newTag = {familyID: $stateParams.family_id}
  $scope.setTag = function(){
    $scope.newTag.token = $scope.key
    Contacts.setTags($scope.newTag, function(){
      $scope.getTag()
      $('.modal').modal('hide')
    })
  }

  Auth.login({username: 'testuser6', password: 'Nzfsg996'}, function(res){
    $scope.key = res.list
    $scope.getLoanInfo()
    $scope.getFamilyAddressInfo()
    $scope.getClientInfo()
    $scope.getFamilyInfo()
    $scope.getNotes()
    $scope.getRelations()
    $scope.getTag()
    $scope.initSearch('searchAddr')
  })


})