// https://testapi.nzfsg.co.nz/Login?username=testuser6&password=Nzfsg996
// app.endpoint = 'https://testapi.nzfsg.co.nz/'

app.endpoint = 'https://testapi.nzfsg.co.nz'

app.factory('Auth', function($resource) {
  return $resource( app.endpoint+'/:action', { action: '@action'}, {
    login: { method:'POST', params: { action: 'Login' }, isArray: false, transformResponse:  function (data) {return {list: angular.fromJson(data)}} }
  });
});

app.factory('Contacts', ['$resource', function($resource) {
  var resource = {
    getContacts:function(params, success, error) {
      var token = params.token
      delete params.token
      var res = $resource(app.endpoint+'/:command/:action', { command: '@command', action: '@action'}, {
        get : { method:'GET', headers: {'Authorization': 'Bearer ' + token }, params: params, isArray: true}
      });
      return res.get({ command: 'contacts', action: 'MyContactGet' }, success, error)
    },
    getContactsLoanList: function(params, success, error) {
      var token = params.token
      delete params.token
      var res = $resource(app.endpoint+'/:command/:action', { command: '@command', action: '@action'}, {
        get : { method:'GET', headers: {'Authorization': 'Bearer ' + token }, params: params, isArray: true}
      });
      return res.get({ command: 'contacts', action: 'LoanList' }, success, error)
    },
    getClientInfo: function(params, success, error) {
      var token = params.token
      delete params.token
      var res = $resource(app.endpoint+'/:command/:action', { command: '@command', action: '@action'}, {
        get : { method:'GET', headers: {'Authorization': 'Bearer ' + token }, params: params, isArray: true}
      });
      return res.get({ command: 'contacts', action: 'ClientInformGet' }, success, error)
    },
    setClientInfo: function(params, success, error) {
      var token = params.token
      delete params.token
      var res = $resource(app.endpoint+'/:command/:action', { command: '@command', action: '@action'}, {
        post : { method:'POST', headers: {'Authorization': 'Bearer ' + token }, params: params, isArray: true}
      });
      return res.post({ command: 'contacts', action: 'ContactSet' }, success, error)
    },
    getContactFamilyInfo: function(params, success, error) {
      var token = params.token
      delete params.token
      var res = $resource(app.endpoint+'/:command/:action', { command: '@command', action: '@action'}, {
        get : { method:'GET', headers: {'Authorization': 'Bearer ' + token }, params: params, isArray: false}
      });
      return res.get({ command: 'contacts', action: 'ContactFamilyInfoGet' }, success, error)
    },
    getFamilyAddressInfo: function(params, success, error) {
      var token = params.token
      delete params.token
      var res = $resource(app.endpoint+'/:command/:action', { command: '@command', action: '@action'}, {
        get : { method:'GET', headers: {'Authorization': 'Bearer ' + token }, params: params, isArray: true}
      });
      return res.get({ command: 'contacts', action: 'FamilyAddressInformGet' }, success, error)
    },
    setFamilyAddressInfo: function(params, body, success, error) {
      var token = params.token
      delete params.token
      var res = $resource(app.endpoint+'/:command/:action', { command: '@command', action: '@action'}, {
        post : { method:'POST', headers: {'Authorization': 'Bearer ' + token }, params: params, isArray: true}
      });
      return res.post({ command: 'contacts', action: 'ContactFamilyInfoSet' }, body, success, error)
    },
    getNotes: function(params, success, error) {
      var token = params.token
      delete params.token
      var res = $resource(app.endpoint+'/:command/:action', { command: '@command', action: '@action'}, {
        get : { method:'GET', headers: {'Authorization': 'Bearer ' + token }, params: params, isArray: true}
      });
      return res.get({ command: 'contacts', action: 'NoteList' }, success, error)
    },
    setNotes: function(params, body, success, error) {
      var token = params.token
      delete params.token
      var res = $resource(app.endpoint+'/:command/:action', { command: '@command', action: '@action'}, {
        post : { method:'POST', headers: {'Authorization': 'Bearer ' + token }, params: params, isArray: false}
      });
      return res.post({ command: 'contacts', action: 'NoteSet' }, body, success, error)
    },
    getTag: function(params, success, error) {
      var token = params.token
      delete params.token
      var res = $resource(app.endpoint+'/:command/:action', { command: '@command', action: '@action'}, {
        get : { method:'GET', headers: {'Authorization': 'Bearer ' + token }, params: params, isArray: true}
      });
      return res.get({ command: 'contacts', action: 'TaggedList' }, success, error)
    },
    setTags: function(params, success, error) {
      var token = params.token
      delete params.token
      var res = $resource(app.endpoint+'/:command/:action', { command: '@command', action: '@action'}, {
        post : { method:'POST', headers: {'Authorization': 'Bearer ' + token }, params: params, isArray: false}
      });
      return res.post({ command: 'contacts', action: 'TaggedSet' }, success, error)
    },
    getRelations: function(params, success, error) {
      var token = params.token
      delete params.token
      var res = $resource(app.endpoint+'/:command/:action', { command: '@command', action: '@action'}, {
        get : { method:'GET', headers: {'Authorization': 'Bearer ' + token }, params: params, isArray: true}
      });
      return res.get({ command: 'contacts', action: 'RelationshipGet' }, success, error)
    },
    setRelations: function(params, body, success, error) {
      var token = params.token
      delete params.token
      var res = $resource(app.endpoint+'/:command/:action', { command: '@command', action: '@action'}, {
        post : { method:'POST', headers: {'Authorization': 'Bearer ' + token }, params: params, isArray: false}
      });
      return res.post({ command: 'contacts', action: 'RelationshipSet' }, body, success, error)
    },
  }
  return resource
}])
