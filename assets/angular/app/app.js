var app = angular.module('app', ['ngResource', 'ui.router', 'angular-loading-bar'])

app.run(function($http, $state) {
});

app.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
  cfpLoadingBarProvider.includeSpinner = true;
  cfpLoadingBarProvider.spinnerTemplate = '<div id="loading-bar-spinner"><div class="spinner-icon pull-left"></div> <span class="loading-message"></span></div>';
}])