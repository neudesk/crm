/*globals jQuery, $, window, document */


var SystemAlert = {
  info: function(message){
    this.html(message, 'info')
  },
  error: function(message){
    this.html(message, 'danger')
  },
  success: function(message){
    this.html(message, 'success')
  },
  html: function(message, alert){
    $('.sysalerts').remove()
    var uniq_id = this.makeid(),
      html = '',
      speed = 400,
      elem = $('.systemAlert'),
      modal = this.hasModal();

    html += '<div class="no-margin alert alert-'+alert+' fade in alert-dismissable text-left sysalerts" data-id="'+uniq_id+'">'
    html += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'
    html +=  message
    html += '</div>'
    if (modal.length > 0){
      modal.find('.modal-body').prepend(html)
      modal.find('.modal-footer button').prop('disabled', false)
    } else {
      // Cache node
      elem.html('').html(html).show();

      if (elem.position()) {
        if (elem.position().top < $(window).scrollTop()) {
          //scroll up
          $('html, body').animate({
            scrollTop: elem.position().top
          }, speed);
        } else if (elem.position().top + elem.height() > $(window).scrollTop() + (window.innerHeight || document.documentElement.clientHeight)) {
          //scroll down
          $('html, body').animate({
            scrollTop: elem.position().top - (window.innerHeight || document.documentElement.clientHeight) + elem.height() + 15
          }, speed);
        }
      }

    }
    setTimeout(function(){
      $('div[data-id="'+uniq_id+'"]').slideUp(300)
    }, 8000)
  },
  hasModal: function(){
    var modal = $('.modal:visible')
    if (modal.length > 0){
      return modal
    } else {
      return false
    }
  },
  makeid: function(){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }
}

var SystemConfirmation = {

  init: function(message, callback){
    this.callback = callback
    this.message = message
    this.modal = $('#systemConfirmModal')
    this.createConfirmationBox()
  },

  createConfirmationBox: function(){
    var cls = this
    cls.modal.find('.modal-body p').text(this.message)
    cls.modal.find('button.continue').attr('onclick', this.callback)
    this.modal.modal('show')
  }

}

var getUrlParameter =  function(param, dummyPath) {
  var sPageURL = dummyPath || window.location.search.substring(1),
    sURLVariables = sPageURL.split(/[&||?]/),
    res = '';

  for (var i = 0; i < sURLVariables.length; i += 1) {
    var paramName = sURLVariables[i],
      sParameterName = (paramName || '').split('=');

    if (sParameterName[0] === param) {
      res = sParameterName[1];
    }
  }

  return res;
}

var unique = function(origArr) {
  var newArr = [],
    origLen = origArr.length,
    found, x, y;

  for (x = 0; x < origLen; x++) {
    found = undefined;
    for (y = 0; y < newArr.length; y++) {
      if (origArr[x] === newArr[y]) {
        found = true;
        break;
      }
    }
    if (!found) {
      newArr.push(origArr[x]);
    }
  }
  return newArr;
}

Number.prototype.pluralize = function(word){
  if (this > 1) {
    return this + ' ' + word + 's'
  } else {
    return this + ' ' + word
  }
}

Array.prototype.remove = function(value) {
  this.splice(this.indexOf(value), 1);
  return true;
};

function percentage(num, per)
{
  return (num/100)*per;
}
;
